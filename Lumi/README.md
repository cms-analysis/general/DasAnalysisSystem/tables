# Lumi mask files (a.k.a. Golden JSON files)

## For LowPU data

### 2018

Copied from

```bash
/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions18/13TeV/PromptReco/Cert_318939-319488_13TeV_PromptReco_SpecialCollisions18_JSON_LOWPU.txt
```

### 2017C

Copied from

```
/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions17/13TeV/ReReco/Cert_299996-303819_13TeV_EOY2017ReReco_Collisions17_JSON_LowPU.txt
```

### 2017H

Copied from

```bash
/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions17/13TeV/ReReco/Cert_306896-307082_13TeV_EOY2017ReReco_Collisions17_JSON_LowPU.txt
```

### 2016

Currently, the 2016 lowPU data is not availible.

