# PU cleaning tables

The tables contain the largest value of transverse momentum at generator level per event ID in the Minimum Bias sample. It can be used as argument to the `applyPUcleaning` command.

To obtain such tables, produce n-tuples from the MinBias sample that was used to simulate the pileup of the samples of interest, then use `getHighScalePUeventIDs` on the raw ntuples.

To find the right MinBias sample, find a PPD expert to guide you through the follow recipe: 
- check the *Chained Request* and find the *DRPremix* request;
- the Premix (a NeutrinoGun sample) is then given by the so-called `pileup_dataset` property of that request;
- repeat the two first steps with the Premix to find the MinBias sample.

Note: for the UL18 jet data, the tables were produced with command `getHighScalePUeventIDs` with 9 as threshold. A cut of `3*pthat` (`2*pthat`) was used in `getHighScalePUeventIDs.cc` (`applyPUcleaning.cc`, only if the rec jet pT is larger than 156 GeV---this is subject to changes).

## Large file storage

The tables being rather large, please use git LFS to handle them. Here is a brief recipe on how to proceed:
- If you have never used git LFS and if it is not installed (it should be done automatically by the installer from [2e22f769](https://gitlab.cern.ch/cms-analysis/general/DasAnalysisSystem/gitlab-profile/-/merge_requests/24/diffs?commit_id=2e22f769c3e3a8c376aa4784a90e1c437d17d697)), run first of all `git lfs install` (do this once per local repo).
- If you want to pull an existing file, run `git lfs pull`.
- If you want to push a new file, first use `git lfs track [file]` (do this once per file), then handle the file as usual with `git add` and `git commit` (do this each time you change the file).
