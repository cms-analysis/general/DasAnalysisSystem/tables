# PU profile tables

Before blindly using the files provided here, one should check the current recommendations [TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData).  
The `Run201*.json` files provided here correspond to the UL recommendations for Run 2 analysis (2016, 2017, 2018) and are copies of the files found under `/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions1?/13TeV/PileUp/UltraLegacy/pileup_latest*.txt`.  
These files can be used ontop data n-tuples to store the pile-up information per event (in principle per lumi section, in practice per event) via:  
 - the stand alone `applyPUlatest` executable  
 - the `mergeNtuples` executable, which contains the `applyPUlatest` correction
