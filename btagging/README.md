# B-tagging tables

The [scale factors](https://btv-wiki.docs.cern.ch/ScaleFactors/) were provided by BTV. The UL convention for the working points (L,M,T) had to be changed to the pre-UL convention to be usable with the `BTagCablibration` implementation (0,1,2).
