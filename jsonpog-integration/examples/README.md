# Examples

Basic examples for standard usage of JSON tables with `correctionlib` are provided.

## BTV

## Electrons

## JERC

Two versino of the example are provided: one in C++ and one in Python. Both essentially do the same. The Python example can be directly executed in the command line, whereas the C++ example should first be compiled with `make`.

Six use cases are shown:
- Retrieve the jet energy correction at a single level (e.g. `L2Relative`) for AK4 and AK8.
- Retrieve the jet energy compound correction (all levels---this is probably what you need in most cases).
- Retrieve the jet energy uncertainty (e.g. `Total`---probably also what most analyses will need).
- Retrieve the jet energy resolution scale factor
- Retrieve the jet energy resolution 
- Retrieve the jet energy correction factor from the jet energy resolution smearing

## JMAR

## MET $\phi$

## Muons

## Tauons

