### HLT_HIAK4PFJet.info
- Dataset:/FSQJet[1,2]/Run2017H-09Aug2019_UL2017_LowPU-v1/MINIAOD
- HLT menu: [/cdaq/physics/Run2017/WMassRun/v3.0.0/HLT/V5](https://cmshltcfg.app.cern.ch/open?cfg=%2Fcdaq%2Fphysics%2FRun2017%2FWMassRun%2Fv3.0.0%2FHLT%2FV5&db=online)
```python
triggerNames += ['HLT_HIAK4PFJet15_v','HLT_HIPFJet25_v','HLT_HIAK4PFJet40_v','HLT_HIAK4PFJet60_v','HLT_HIAK4PFJet80_v','HLT_HIPFJet140_v']
```
### HLT_AK4PFJet.info
- Dataset: /FSQJet[1,2]/Run2017C-09Aug2019_UL2017_LowPU-v1/MINIAOD
- HLT menu: [/cdaq/physics/Run2017/LowPU25ns/v2.0/HLT/V1](https://cmshltcfg.app.cern.ch/open?cfg=/cdaq/physics/Run2017/LowPU25ns/v2.0/HLT/V1&db=online)
```python
triggerNames += ['HLT_AK4PFJet30_v','HLT_AK4PFJet50_v','HLT_AK4PFJet80_v','HLT_AK4PFJet100_v','HLT_AK4PFJet120_v']
```