# Trigger lumi
The `Triggerlumi.info` are used to normalise the data samples to the luminosity. 
More technically, can be used by `getTriggerCurves`, `getTriggerTurnons` and `applyDataNormalisation`.

These trigger thresholds was produced by `brilcalc`.

## Example
Command use:
```
brilcalc lumi --normtag /cvmfs/cms-bril.cern.ch/cms-lumi-pog/Normtags/normtag_PHYSICS.json -u /pb -i processedLumis.json --hltpath HLT_AK4PFJet30_v*
```

Here the `processedLumis.json` is the sum of all FSQJet samples' `processedLumis.json` files.
The `processedLumis.json` was obtained by running `crab report` on the crab output directory for each era.

[BrilcalcQuickStart](https://twiki.cern.ch/twiki/bin/view/CMS/BrilcalcQuickStart)
